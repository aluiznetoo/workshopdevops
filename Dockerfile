FROM python:2.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

ENV http_proxy host:port
ENV https_proxy host:port

ENV http_proxy="http://:bkuounipe45@10.10.32.1:3128"
ENV https_proxy="http://:bkuounipe45@10.10.32.1:3128"


# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
